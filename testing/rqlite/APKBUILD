# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=rqlite
pkgver=7.20.5
pkgrel=0
pkgdesc="Lightweight, distributed relational database built on SQLite"
url="https://github.com/rqlite/rqlite"
arch="all !riscv64" # ftbfs
license="MIT"
makedepends="go sqlite-dev"
checkdepends="python3 py3-requests"
options="chmod-clean !check" # a bunch of them fail due to runtime requirements
subpackages="
	$pkgname-doc
	$pkgname-bench
	$pkgname-client
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/rqlite/rqlite/archive/refs/tags/v$pkgver.tar.gz"

export GOFLAGS="$GOFLAGS -tags=libsqlite3"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build ./cmd/rqbench
	go build ./cmd/rqlite
	go build ./cmd/rqlited
}

check() {
	RQLITED_PATH="$builddir"/rqlited python3 system_test/full_system_test.py
}

package() {
	install -Dm755 rqbench rqlite rqlited \
		-t "$pkgdir"/usr/bin

	install -D -m644 "$builddir"/DOC/*.md -t \
		"$pkgdir"/usr/share/doc/$pkgname
}

bench() {
	pkgdesc="$pkgdesc (benchmark utility)"

	amove usr/bin/rqbench
}

client() {
	pkgdesc="$pkgdesc (client)"

	amove usr/bin/rqlite
}

sha512sums="
a75f5e2102c6d478162b499e6252ade3117c8259f66ad5d7434b72355465214f7bdf5a52ecce74c0916b31b82dc0b829e396a4f8a1d2dcba106405a55cfa3f9a  rqlite-7.20.5.tar.gz
"
