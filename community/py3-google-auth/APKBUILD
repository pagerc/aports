# Contributor: Keith Maxwell <keith.maxwell@gmail.com>
# Maintainer: Keith Maxwell <keith.maxwell@gmail.com>
pkgname=py3-google-auth
_pyname=google-auth
pkgver=2.20.0
pkgrel=0
pkgdesc="Google authentication library for Python."
url="https://google-auth.readthedocs.io/en/latest/"
arch="noarch"
license="Apache-2.0"
depends="
	python3
	py3-cachetools
	py3-asn1-modules
	py3-rsa
	py3-six
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="
	py3-certifi
	py3-cryptography
	py3-flask
	py3-freezegun
	py3-grpcio
	py3-mock
	py3-oauth2client
	py3-openssl
	py3-pytest
	py3-pytest-cov
	py3-pytest-localserver
	py3-pyu2f
	py3-requests
	py3-responses
	py3-urllib3
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/g/google-auth/google-auth-$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
e13471f4452bd5c69c2ca9d1ff145431b160805e4ab2d3a70c35cad78c6e3e4f4bc789dfe99f55a0e60e5f335f9983529eabbfb0fc50794b62dfa7eeb70882b1  google-auth-2.20.0.tar.gz
"
